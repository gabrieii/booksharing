'use strict';

module.exports = function (grunt) {
  // Show elapsed time at the end
  //require('time-grunt')(grunt);
  // Load all grunt tasks
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-express-server');
  require('load-grunt-tasks')(grunt);
  // Project configuration.
  grunt.initConfig({
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      server: {
        src: ['./app.js', './routes/*.js']
      }
    },
    express: {
      myServer: {
        options: {
          script: './app.js'
        }
      },
    },
    watch: {
      express: {
        options: {
          spawn: false    
        },
        files: ['./app.js','./routes/*.js'],
        tasks: ['express:myServer','jshint:server']
      },
      gruntfile: {
        files: './Gruntfile.js',
        tasks: ['jshint:gruntfile', 'express:myServer']
      }
    }
  });
  // Default task.
  grunt.registerTask('default', ['express:myServer', 'watch', 'jshint']);
};
'use strict';

/**
 * @ngdoc overview
 * @name ibmApp
 * @description
 * # ibmApp
 *
 * Main module of the application.
 */
angular
  .module('ibmApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap'
  ])
  .config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
     // $urlRouterProvider.otherwise('startingpage');
      $urlRouterProvider.when('', 'startingpage');

      $stateProvider
        .state('navbar', {
          url: '',
          template: '<navbar></navbar><ui-view/>'
        })
        .state('startingPage', {
          url: '/startingpage',
          templateUrl: 'views/startingpage.html',
          controller: 'StartingpageCtrl',
          parent: 'navbar'
        })
        .state('home', {
          url: '/home',
          templateUrl: 'views/home.html',
          controller: 'HomeCtrl',
          parent: 'navbar'
        })
        .state('home.addBook', {
          url: '/addbook',
          templateUrl: 'views/addbk.html',
          controller: 'AddbkCtrl'
        })
        .state('home.addNewBook', {
          url: '/addnewbook',
          templateUrl: 'views/addbook.html',
          controller: 'AddbookCtrl'
        })
        .state('home.book', {
          url: '/book/:bookId',
          templateUrl: 'views/book.html',
          controller: 'BookCtrl',
        })
        .state('home.bookOwners', {
          url: '/bookowners/:bookId',
          templateUrl: 'views/bookowners.html',
          controller: 'BookownersCtrl'
        })
        .state('home.getBook', {
          url: '/getbook',
          templateUrl: 'views/getbook.html',
          controller: 'GetbookCtrl'
        })
        .state('home.recommendations', {
          url: '/recommendations',
          templateUrl: 'views/recommendations.html',
          controller: 'RecommendationsCtrl'
        })
        .state('home.notifications', {
          url: '/notifications',
          templateUrl: 'views/notifications.html',
          controller: 'NotificationsCtrl'
        })
        .state('home.myBooks', {
          url: '/mybooks',
          templateUrl: 'views/mybooks.html',
          controller: 'MybooksCtrl'
        })
        .state('home.rankings', {
          url: '/rankings',
          templateUrl: 'views/rankings.html',
          controller: 'RankingsCtrl'
        })
        .state('home.help', {
          url: '/help',
          templateUrl: 'views/help.html',
          controller: ''
        })
        .state('signup', {
          url: '/signup',
          templateUrl: 'views/signup.html',
          controller: 'SignupCtrl'
        });
    }
]);

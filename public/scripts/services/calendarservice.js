'use strict';

/**
 * @ngdoc service
 * @name ibmApp.CalendarService
 * @description
 * # CalendarService
 * Factory in the ibmApp.
 */
angular.module('ibmApp')
  .factory('CalendarService',
    ['$q',
    '$http',
    '$cookieStore',
    '$timeout',
    'SERVER',

    function ($q, $http, $cookieStore, $timeout, SERVER) {
    // Service logic
    // ...

    // Public API here
    return {
      getSlots: function (date) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        // $timeout(function() {
        //   deferred.resolve([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        // }, 100);
        console.log(date);

        $http.get(SERVER.URL + SERVER.CALENDAR, {
          params: {
            token: token,
            date: date
          }
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        })
        return deferred.promise;
      },
      getAlarm: function() {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.get(SERVER.URL + SERVER.CALENDAR + '/alarm', {
          params: {
            token: token
          }
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      },
      getReminder: function() {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.get(SERVER.URL + SERVER.CALENDAR + '/reminder', {
          params: {
            token: token
          }
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      }
    };
  }
]);

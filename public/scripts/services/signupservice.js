'use strict';

/**
 * @ngdoc service
 * @name ibmApp.SignUpService
 * @description
 * # SignUpService
 * Factory in the ibmApp.
 */
angular.module('ibmApp')
  .factory('SignUpService',
    ['$q',
    '$http',
    'SERVER',

    function ($q, $http, SERVER) {
    // Service logic
    // ...


    // Public API here
      return {
        createSignUpObject: function (fields) {
          return {
            firstName  : fields[0].answer,
            lastName   : fields[1].answer,
            username   : fields[2].answer,
            password   : fields[3].answer,
            email      : fields[4].answer,
            phoneNumber: fields[5].answer,
            city       : fields[6].answer
          };
        },
        signUp: function(credentials) {
          var deferred = $q.defer();
          
          $http.post(SERVER.URL + SERVER.SIGN_UP, credentials)
            .success(function(data, status, headers, config) {
              deferred.resolve(data);
            })
            .error(function(data) {
              deferred.reject(data);
            });
          return deferred.promise;
        }

      };
  }
]);

'use strict';

/**
 * @ngdoc service
 * @name ibmApp.BookingService
 * @description
 * # BookingService
 * Factory in the ibmApp.
 */
angular.module('ibmApp')
  .factory('BookingService', 
    ['$q',
    '$http',
    '$cookieStore',
    'SERVER',

    function ($q, $http, $cookieStore, SERVER) {
    // Service logic
    // ...

    // Public API here
    return {
      getBookingList: function() {
        return ['Petrecere', 'Intalnire', 'Curs', 'Sport', 'Altele'];
      },
      sendBooking: function (booking) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        console.log('cucubau')

        $http.post(SERVER.URL + SERVER.CALENDAR + SERVER.NEW_TASK, {
          token: token,
          date: booking.date,//string parsat
          start: booking.start,
          stop: booking.stop,
          alarmChecked: booking.alarmChecked,//booleana
          alarmDelay: booking.alarmDelay,
          reminderChecked: booking.reminderChecked,
          reminderDelay: booking.reminderDelay,
          taskType: booking.taskType,
          note: booking.note,
          title: booking.title
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      },
      cancelBooking: function(booking) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.post(SERVER.URL + SERVER.CALENDAR + '/canceltask', {
          token: token,
          date: booking.date,//parsat
          start: booking.from,//indici matrice
          stop: booking.until
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      },
      doneBooking: function(booking) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.post(SERVER.URL + SERVER.CALENDAR + '/dotask', {
          token: token,
          date: booking.date,
          start: booking.from,
          stop: booking.until
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      }
    };
  }
]);

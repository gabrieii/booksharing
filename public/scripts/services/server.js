'use strict';

/**
 * @ngdoc service
 * @name ibmApp.SERVER
 * @description
 * # SERVER
 * Constant in the ibmApp.
 */
angular.module('ibmApp')
  .constant('SERVER', {
  	URL: 'http://10.100.100.38:1337',
  	SIGN_UP: '/signup',
  	LOG_IN: '/login',
  	LOG_OUT: '/logout',
  	USER_INFO: '/userinfo',
  	BOOK: '/book',
    ALL_BOOKS: '/book/all',
    BOOK_OWNERS: '/book/users',
 	BOOK_REQUEST: '/book/request',
 	ADD_BOOK: '/book/addpersonal'
 });

'use strict';

/**
 * @ngdoc service
 * @name ibmApp.DateService
 * @description
 * # DateService
 * Factory in the ibmApp.
 */
angular.module('ibmApp')
  .factory('DateService', function () {
    // Service logic
    // ...

    // Public API here
    return {
      parseDate: function (date) {
        return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
      }
    };
  });

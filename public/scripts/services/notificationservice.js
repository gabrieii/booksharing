'use strict';

/**
 * @ngdoc service
 * @name ibmAppApp.NotificationService
 * @description
 * # NotificationService
 * Factory in the ibmAppApp.
 */
angular.module('ibmApp')
  .factory('NotificationService', function ($q, $http, $cookieStore, SERVER) {
    // Service logic
    // ...
    return {
      getNotifications: function() {
          var deferred = $q.defer();
          var token = $cookieStore.get('token');
          //var token = '123';
          // $timeout(function() {
          //   deferred.resolve({
          //     firstName: 'Gabi'
          //   });
          // }, 1000);
          $http.get(SERVER.URL + SERVER.USER_INFO, { params: { token: token, single: 'notifications' } })
           .success(function(data) {
              deferred.resolve(data);
           })
           .error(function(data) {
              deferred.reject(data);
           });
          return deferred.promise;
      },
      answerNotification: function(id, answer) {
          var deferred = $q.defer();
          var token = $cookieStore.get('token');

          $http.post(SERVER.URL + '/notifications/accept', {
            token: token,
            accepted: answer,
            notificationID: id
          })
          .success(function(data) {
              deferred.resolve(data);
          })
          .error(function(data) {
              deferred.reject(data);
           });
          return deferred.promise;
      },
      markAsRead: function(id) {
          var deferred = $q.defer();
          var token = $cookieStore.get('token');

          $http.post(SERVER.URL + '/notifications/read', {
            token: token,
            notificationID: id
          })
          .success(function(data) {
              deferred.resolve(data);
          })
          .error(function(data) {
              deferred.reject(data);
           });
          return deferred.promise;
      },
      sendRating: function(id, user, rating) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.post(SERVER.URL + '/rating', {
          token: token,
          notificationID: id,
          username: user,
          rating: rating
        })
        .success(function(data) {
            deferred.resolve(data);
        })
        .error(function(data) {
            deferred.reject(data);
         });
        return deferred.promise;
      }
    };
  });

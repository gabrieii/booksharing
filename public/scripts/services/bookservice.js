'use strict';

/**
 * @ngdoc service
 * @name ibmAppApp.BookService
 * @description
 * # BookService
 * Factory in the ibmAppApp.
 */
angular.module('ibmApp')
  .factory('BookService', function ($q, $http, $cookieStore, SERVER) {
    // Service logic
    // ...
    return {
      getBookList: function() {
        var deferred = $q.defer();

        $http.get(SERVER.URL + SERVER.ALL_BOOKS)
          .success(function(data) {
            deferred.resolve(data);
          })
          .error(function(data) {
            deferred.reject(data);
          });
        return deferred.promise;
      },
      getBook: function(id) {
        var deferred = $q.defer();

        console.log(id)

        $http.get(SERVER.URL + SERVER.BOOK, { params : { ISBN: id }})
          .success(function(data) {
            deferred.resolve(data);
          })
          .error(function(data) {
            deferred.reject(data);
          });
        return deferred.promise;
      },
      getBookOwners: function(id) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.get(SERVER.URL + SERVER.BOOK_OWNERS, { params: { token: token, ISBN: id }})
          .success(function(data) {
            deferred.resolve(data);
          })
          .error(function(data) {
            deferred.reject(data);
          });
        return deferred.promise;
      },
      requestBook: function(user, bookId) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.post(SERVER.URL + SERVER.BOOK_REQUEST, {
          token: token,
          username: user,
          ISBN: bookId
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        })
        return deferred.promise;
      },
      addBook: function(book) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.post(SERVER.URL + SERVER.ADD_BOOK, {
          token: token,
          title: book.title,
          author: book.author,
          description: book.description,
          category: book.category,
          cover: book.cover,
          ISBN: book.ISBN
        })
          .success(function(data) {
            deferred.resolve(data);
          })
          .error(function(data) {
            deferred.reject(data);
          });
        return deferred.promise;
      },
      getRecommendations: function() {
        var token = $cookieStore.get('token');
        var deferred = $q.defer();

        $http.get(SERVER.URL + '/recommend', { params: { token: token } })
          .success(function(data) {
            deferred.resolve(data);
          })
          .error(function(data) {
            deferred.reject(data);
          });
        return deferred.promise;
      },
      getBookListToAdd: function() {
        var token = $cookieStore.get('token');
        var deferred = $q.defer();

        $http.get(SERVER.URL + SERVER.BOOK + '/exceptpersonal', { params: { token: token } })
          .success(function(data) {
            deferred.resolve(data);
          })
          .error(function(data) {
            deferred.reject(data);
          });
        return deferred.promise;
      }
    };
  });

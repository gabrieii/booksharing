'use strict';

/**
 * @ngdoc service
 * @name ibmApp.AuthService
 * @description
 * # AuthService
 * Factory in the ibmApp.
 */
angular.module('ibmApp')
  .factory('AuthService', 
    ['$http',
    '$q',
    '$cookieStore',
    '$timeout',
    'SERVER',

    function ($http, $q, $cookieStore, $timeout, SERVER) {
    // Service logic
    // ...

    // Public API here
      return {
        login: function (credentials) {
          var deferred = $q.defer();
          console.log(credentials);

          // $timeout(function() {
          //   deferred.resolve({
          //     firstName: 'Gabi'
          //   });
          // }, 1000);

          $http.post(SERVER.URL + SERVER.LOG_IN, credentials)
            .success(function(data) {
              console.log(data.token)
              $cookieStore.put('token', data.token);
              deferred.resolve(data);
            })
            .error(function(data) {
              deferred.reject(data);
            });
          return deferred.promise;
        },
        logout: function() {
          var deferred = $q.defer();
          var token = $cookieStore.get('token');

          // $timeout(function() {
          //   deferred.resolve();
          // }, 1000);
          $http.post(SERVER.URL + SERVER.LOG_OUT, { token: token })
            .success(function(data) {
              $cookieStore.put('token', '');
              deferred.resolve(data);
            })
            .error(function(data) {
              deferred.reject(data);
            });
          return deferred.promise;
        },
        getUserDetails: function(detail) {
          var deferred = $q.defer();
          var _token = '-1';
          try {
            _token = $cookieStore.get('token');
          } catch(exception) {

          }
          //var token = '123';
          // $timeout(function() {
          //   deferred.resolve({
          //     firstName: 'Gabi'
          //   });
          // }, 1000);
          console.log(detail)
          $http.get(SERVER.URL + SERVER.USER_INFO, { params: { token: _token, single: detail } })
           .success(function(data) {
              deferred.resolve(data);
           })
           .error(function(data) {
              deferred.reject(data);
           });
          return deferred.promise;
        },
        getMood: function() {
          var deferred = $q.defer();
          var token = $cookieStore.get('token');

          $http.get(SERVER.URL + '/mood', {
            params: {
              token: token
            }
          }).success(function(data) {
            deferred.resolve(data);
          }).error(function(data) {
            deferred.reject(data);
          });
          return deferred.promise;
        }
      };
  }
]);

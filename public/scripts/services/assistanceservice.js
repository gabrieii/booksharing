'use strict';

/**
 * @ngdoc service
 * @name ibmApp.AssistanceService
 * @description
 * # AssistanceService
 * Factory in the ibmApp.
 */
angular.module('ibmApp')
  .factory('AssistanceService',
    ['$q',
    '$http',
    '$cookieStore',
    'SERVER',

    function ($q, $http, $cookieStore, SERVER) {
    // Service logic
    // ...

    // Public API here
    return {
      askQuestion: function (question) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.get(SERVER.URL + SERVER.ASSISTANT, {
          params: {
            token: token,
            input: question
          }
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      }
    };
  }
]);

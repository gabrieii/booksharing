'use strict';

/**
 * @ngdoc directive
 * @name ibmApp.directive:navbar
 * @description
 * # navbar
 */
angular.module('ibmApp')
  .directive('navbar',
    ['$state',
    '$interval',
    'AuthService',
    'CalendarService',

    function ($state, $interval, AuthService, CalendarService) {
      return {
        templateUrl: 'views/navbar.html',
        restrict: 'E',
        link: function postLink(scope, element, attrs) {
          scope.image = null;
          scope.message = '';

          var clearCredentials = function() {
            scope.credentials.username = '';
            scope.credentials.password = '';
          };

          var update = function() {
            alert('bravo ma')
          };

          scope.user = null;

          AuthService.getUserDetails()
          .then(function(data) {
            scope.user = data;
            console.log(scope.user);
          }, function(data) {
            scope.user = null;
          });

          scope.credentials = {
            username: '',
            password: ''
          };

          scope.login = function() {
            AuthService.login(scope.credentials)
            .then(function(data) {
              clearCredentials();
              // update();
              console.log('baubau')
              $state.go('home.getBook');
            }, function() {
              alert('bad username or password');
            });
          };
  
          scope.logout = function() {
            AuthService.logout()
            .then(function() {
              scope.user = null;
              scope.image = '';
              $state.go('startingPage');
            }, function() {
              alert('Eroare la log out');
            });
          };

          scope.$on('gotUserDetails', function(event, data) {
            console.log(data);
            scope.user = data;
          });
        }
      };
    }
]);


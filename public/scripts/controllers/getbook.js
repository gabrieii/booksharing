'use strict';

/**
 * @ngdoc function
 * @name ibmAppApp.controller:GetbookCtrl
 * @description
 * # GetbookCtrl
 * Controller of the ibmAppApp
 */
angular.module('ibmApp')
  .controller('GetbookCtrl', function ($scope, $state, BookService) {
   	$scope.books = [];
    $scope.searchText = '';

   	BookService.getBookList().then(function(data) {
   		$scope.books = data;
   	});

   	$scope.seeBook = function(id) {
   		$state.go('home.book', { bookId : id });
   	};
  });

'use strict';

/**
 * @ngdoc function
 * @name ibmAppApp.controller:MybooksCtrl
 * @description
 * # MybooksCtrl
 * Controller of the ibmAppApp
 */
angular.module('ibmApp')
  .controller('MybooksCtrl', function ($scope, $timeout, AuthService) {
  	$scope.books = [];
  	$scope.message = '';

  	console.log('ipika')
  	AuthService.getUserDetails('personalBooks').then(function(data) {
  		$scope.books = data;
  	});

  	$scope.$on('bookAdded', function(event) {
  		event.preventDefault();
  		$scope.message = 'The book has successfully been added!';
  		console.log($scope.message);
  		$timeout(function() {
  			$scope.message = '';
  		});
  	});
  });

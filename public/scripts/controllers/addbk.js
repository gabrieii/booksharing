'use strict';

/**
 * @ngdoc function
 * @name ibmAppApp.controller:AddbkCtrl
 * @description
 * # AddbkCtrl
 * Controller of the ibmAppApp
 */
angular.module('ibmApp')
  .controller('AddbkCtrl', function ($scope, $state, BookService) {
   	$scope.books = [];
   	$scope.searchText = '';

   	BookService.getBookListToAdd().then(function(data) {
   		$scope.books = data;
   	});

   	$scope.addBook = function(book) {
      console.log(book)
      BookService.addBook(book).then(function(data) {
   		//t0d0 - book has successfully been added
        $state.go('home.myBooks');
        $scope.$emit('bookAdded');
     	}, function(data) {
     		//t0d0 - book has not been added
     	});
    };
  });

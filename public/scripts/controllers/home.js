'use strict';

/**
 * @ngdoc function
 * @name ibmApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the ibmApp
 */
angular.module('ibmApp')
  .controller('HomeCtrl',
  	['$scope',
    '$state', 
    '$interval', 	
    'AuthService',
    'NotificationService',

  	function ($scope, $state, $interval, AuthService, NotificationService) {
  		$scope.user = null;
      $scope.notificationsCount = 0;

   		AuthService.getUserDetails()
   		.then(function(data) {
   			console.log(data)
        $scope.$emit('gotUserDetails', data);
   			$scope.user = data;	
   		}, function(error) {
        // $state.go('startingPage');
      });

      var getNotificationsCount = function() {  
        NotificationService.getNotifications().then(function(data) {
          $scope.notificationsCount = data.length;
        });
      };

      getNotificationsCount();
      $interval(function() {
        getNotificationsCount();
      }, 5000);
  	}
]);

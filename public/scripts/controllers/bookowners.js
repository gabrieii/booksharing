'use strict';

/**
 * @ngdoc function
 * @name ibmAppApp.controller:BookownersCtrl
 * @description
 * # BookownersCtrl
 * Controller of the ibmAppApp
 */
angular.module('ibmApp')
  .controller('BookownersCtrl', function ($scope, $state, $stateParams, BookService) {
    $scope.owners = [];
    $scope.book = null;

    BookService.getBook($stateParams.bookId).then(function(data) {
   		$scope.book = data;
   	});

   	BookService.getBookOwners($stateParams.bookId).then(function(data) {
   		$scope.owners = data;
      for (var i = 0; i < $scope.owners.length; ++i) {
        $scope.owners[i].rating = Math.round($scope.owners[i].rating);
      }
   	});

    $scope.sendRequest = function(user) {
      BookService.requestBook(user, $stateParams.bookId).then(function(data) {
        $state.go('home.getBook');
      });
    }
  });

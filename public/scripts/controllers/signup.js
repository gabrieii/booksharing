'use strict';

/**
 * @ngdoc function
 * @name ibmApp.controller:SignupCtrl
 * @description
 * # SignupCtrl
 * Controller of the ibmApp
 */
angular.module('ibmApp')
  .controller('SignupCtrl',
  	['$scope',
    '$state',
    'SignUpService',

  	function ($scope, $state, SignUpService) {
    	$scope.fields = [{
    		text: 'First Name',
    		answer: ''
    	}, {
    		text: 'Last Name',
    		answer: ''
    	}, {
    		text: 'Username',
    		answer: ''
    	}, {
    		text: 'Password',
    		answer: ''
    	}, {
    		text: 'Email',
    		answer: ''
    	}, {
            text: 'Phone',
            answer: ''
        }, {
            text: 'City', 
            answer: ''
        }];

    	$scope.signup = function() {
            var signUpObject = SignUpService.createSignUpObject($scope.fields);
            SignUpService.signUp(signUpObject)
            .then(function() {
              $state.go('startingPage');
            }, function(error) {
              alert(error);
            });
    	};
  	}
]);

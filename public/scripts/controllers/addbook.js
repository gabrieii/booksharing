'use strict';

/**
 * @ngdoc function
 * @name ibmAppApp.controller:AddbookCtrl
 * @description
 * # AddbookCtrl
 * Controller of the ibmAppApp
 */
angular.module('ibmApp')
  .controller('AddbookCtrl', function ($scope, $state, BookService) {
    $scope.book = {
    	title: '',
    	author: '',
    	description: '',
    	category: '',
    	cover: '',
    	ISBN: ''
    };

    $scope.submitBook = function() {
    	console.log('buyaka')
    	BookService.addBook($scope.book).then(function(data) {
    		//t0d0 mesaj success
            $state.go('home.myBooks');
    	}, function(data) {
    		//t0d0 mesaj eroare
            alert('mare eroare')
    	});
    };	
  });

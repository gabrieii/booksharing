'use strict';

/**
 * @ngdoc function
 * @name ibmApp.controller:RecommendationsCtrl
 * @description
 * # RecommendationsCtrl
 * Controller of the ibmApp
 */
angular.module('ibmApp')
  .controller('RecommendationsCtrl',
  	['$scope',
    '$state',
  	'BookService',

  	function ($scope, $state, BookService) {
  		$scope.books = [];

  		BookService.getRecommendations().then(function(data) {
  			$scope.books = data;
  			console.log(data)
  		});

      $scope.seeBook = function(id) {
        $state.go('home.book', { bookId : id });
      };
  	}
]);

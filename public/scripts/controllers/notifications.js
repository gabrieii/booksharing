'use strict';

/**
 * @ngdoc function
 * @name ibmAppApp.controller:NotificationsCtrl
 * @description
 * # NotificationsCtrl
 * Controller of the ibmAppApp
 */
angular.module('ibmApp')
  .controller('NotificationsCtrl', function ($scope, $interval, AuthService, NotificationService) {
   	$scope.notifications = [];
    $scope.rating = 4;

   	var getNotifications = function() {
	   	AuthService.getUserDetails('notifications').then(function(data) {
	   		if (data.length !== $scope.notifications.length) {
          $scope.notifications = data;
        }
	   	});
   	};

    $scope.doshit = function() {
      console.log($scope.rating)
    }

   	getNotifications();

   	$interval(function() {
   		getNotifications();
   	}, 5000);

   	$scope.answerRequest = function(id, answer) {
   		NotificationService.answerNotification(id, answer).then(function(data) {
   			getNotifications();
   		});
   	};

    $scope.markAsRead = function(id) {
      NotificationService.markAsRead(id).then(function(data) {
        getNotifications();
      });
    };

    $scope.sendRating = function(id, user) {
      console.log($scope.rating)
      NotificationService.sendRating(id, user, $scope.rating).then(function(data) {
        // $scope.rating = 0;
      });
    };
  });

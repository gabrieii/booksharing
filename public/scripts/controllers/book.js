'use strict';

/**
 * @ngdoc function
 * @name ibmAppApp.controller:BookCtrl
 * @description
 * # BookCtrl
 * Controller of the ibmAppApp
 */
angular.module('ibmApp')
  .controller('BookCtrl', function ($scope, $state, $stateParams, BookService) {
   	$scope.book = null;

   	BookService.getBook($stateParams.bookId).then(function(data) {
   		$scope.book = data;
   	});

   	$scope.goToOwners = function() {
   		$state.go('home.bookOwners', { bookId: $stateParams.bookId });
   	};
  });

'use strict';

/**
 * @ngdoc filter
 * @name ibmApp.filter:floor
 * @function
 * @description
 * # floor
 * Filter in the ibmApp.
 */
angular.module('ibmApp')
  .filter('floor', function () {
    return function (input) {
      return Math.floor(input);
    };
  });

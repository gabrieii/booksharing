'use strict';

/**
 * @ngdoc filter
 * @name ibmApp.filter:halfhour
 * @function
 * @description
 * # halfhour
 * Filter in the ibmApp.
 */
angular.module('ibmApp')
  .filter('halfhour', function () {
    return function (input) {
      return (input % 2) * 3;
    };
  });

'use strict';

/**
 * @ngdoc filter
 * @name ibmAppApp.filter:notificationFilter
 * @function
 * @description
 * # notificationFilter
 * Filter in the ibmAppApp.
 */
angular.module('ibmApp')
  .filter('notificationFilter', function () {
    return function (notification) {
      var notificationType = notification.type;
      switch(notificationType) {
      	case 'request' : return 'You have a request from ' + notification.user.firstName + ' ' + notification.user.lastName;
      	case 'accept'  : return 'Your request has been accepted by '+ notification.user.firstName + ' ' + notification.user.lastName;
      	case 'decline' : return 'Your request has been declined by'+ notification.user.firstName + ' ' + notification.user.lastName;
      	case 'rating'  : return 'Rate ' + notification.user.firstName + ' ' + notification.user.lastName;
        default        : return '';
      } 
    };
  });

var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);

router.get('/', function(request, result) {
	var users = db.get("users");
	users.find({}, function(err, allUsers) {
		if (err) { result.send(err); }
		else {
			var clasament = [];
			for (var i = 0; i < allUsers.length; i++) {
				clasament[i] = {
					score : 0,
					user : allUsers[i].username
				};
				if (allUsers[i].borrowedBooks === undefined || allUsers[i].borrowedBooks === null) {
					allUsers[i].borrowedBooks = [];
				}
				console.log(allUsers[i].rating);
				clasament[i].score = (parseInt(allUsers[i].rating) * parseInt(allUsers[i].ratingCounter) + 5) * allUsers[i].borrowedBooks.length;
			}
			console.log(clasament);
			clasament.sort(function(a, b) {
				return b.score - a.score;
			});
			result.send(clasament);
		}
	});
});

module.exports = router;

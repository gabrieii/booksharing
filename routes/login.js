var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);
var crypto = require("crypto");

router.post('/', function(request, result) {
	var username = request.body.username;
	var password = request.body.password;
  	var users = db.get("users");
    console.log(username,password);
  	users.findOne({username : username}, function(err, user) {
        if (err) {  }
        else
        {
            if (user === null || user === undefined) { 
                result.send("Utilizatorul nu exista!");
                return;
            }
            crypto.pbkdf2(password, 'ala-bala-portocala', 10000, 20, function(err, derivedKey) {
                if (err) {}
                else {
                    if (derivedKey.toString() === user.password.buffer.toString()) { //aici o sa fie de fapt user.password.buffer
						var shasum = crypto.createHash('sha1');
			            shasum.update(user._id + Math.ceil(Math.random() * 1000000).toString(), 'utf8');
			            var hash = shasum.digest('hex');
			            users.update({_id : user._id}, {$set : {token : hash}}, function(err) {
			                if ( err ) {  }
			                else { 
			                    result.send({token: hash}); 
			                }
			            });
			            return;
                    }
                    else {
                    	result.status(401).end();
                    }
                }
            });
        }
    });
});

module.exports = router;
var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);

/* GET home page. */
router.get('/', function(req, res) {
  	res.render('index', { title: 'Express' });
});

router.get("/permission", function(request, result) {
	var users = db.get("users");
	var token = request.query.token;
	if (token === "-1") {
		result.send({
			permission : false
		});
	}
	users.findOne({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === null || user === undefined) {
				result.send({
					permission : false
				});
				return;
			}
			result.send({
				permission : true
			});
		}
	});
});

module.exports = router;

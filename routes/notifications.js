var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);

var nodemailer = require('nodemailer');

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'ibm.hackathon.mailer@gmail.com',
        pass: 'automailer11'
    }
});

router.post('/accept', function(request, result) {
	var users = db.get("users");
	var token = request.body.token;
	try {
		token = JSON.parse(token).token;
	} catch (e) {
		try {
			token = JSON.parse(token);
		} catch (exception) {}
	}
	var accepted = request.body.accepted;
	var notificationID = request.body.notificationID;
	try {
		accepted = JSON.parse(accepted);
	} catch (e) {}
	users.findOne({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === undefined || user === null) {
				result.status(404).end();
				return;
			}
			for (var i = 0; i < user.notifications.length; i++) {
				(function(i) {
					if (user.notifications[i].id === notificationID) {
						user.notifications[i].read = true;
						users.update({token : token}, {$set : {notifications : user.notifications}});
						var usernameToBeNotified = user.notifications[i].user.username;
						users.findOne({username : usernameToBeNotified}, function(err, userToBeNotified) {
							if (err) { result.send(err); }
							else {
								if (userToBeNotified === null || userToBeNotified === undefined) {
									result.status(404).end();
									return;
								}
								var crypto = require("crypto");
								var current_date = (new Date()).valueOf().toString();
								var random = Math.random().toString();
								var hash = crypto.createHash('sha1').update(current_date + random).digest('hex');
								var type;
								if (accepted === 1) {
									var ratingNoteForNotifiedUser = {
										type : "rating",
										id : hash + Math.random().toString(),
										user : {
											username : user.username,
											firstName : user.firstName,
											lastName : user.lastName,
											email : user.email,
											phoneNumber : user.phoneNumber,
											rating : user.rating
										},
										read : false
									};
									var ratingNoteForCurrentUser = {
										type : "rating",
										id : hash + Math.random().toString(),
										user : {
											username : userToBeNotified.username,
											firstName : userToBeNotified.firstName,
											lastName : userToBeNotified.lastName,
											email : userToBeNotified.email,
											phoneNumber : userToBeNotified.phoneNumber,
											rating : userToBeNotified.rating
										},
										read : false
									};
									user.notifications.push(ratingNoteForCurrentUser);
									userToBeNotified.notifications.push(ratingNoteForNotifiedUser);
									users.update({username : user.username}, {$set : {notifications : user.notifications}});
									users.update({username : userToBeNotified.username}, {$set : {notifications : userToBeNotified.notifications}});
									type = "accept";
									if (userToBeNotified.borrowedBooks === null || userToBeNotified.borrowedBooks === undefined) {
										userToBeNotified.borrowedBooks = [];
									}
									userToBeNotified.borrowedBooks.push(user.notifications[i].book);
								}
								else {
									type = "decline";
								}
								userToBeNotified.notifications.push({
									type : type,
									id : hash,
									user : {
										username : user.username,
										firstName : user.firstName,
										lastName : user.lastName,
										email : user.email,
										phoneNumber : user.phoneNumber,
										rating : user.rating
									},
									book : user.notifications[i].book,
									read : false
								});
								users.update({username : userToBeNotified.username}, {$set : {notifications : userToBeNotified.notifications}}, function() {
									result.status(200).end();
									if (type === "accept") {
										type = "accepted";
									}
									else {
										type = "declined";
									}
									var mailOptions = {
									    from: 'AutoMailer ✔ <ibm.hackathon.mailer@gmail.com', // sender address
									    to: userToBeNotified.email, // list of receivers
									    subject: 'Booksharing - status of request', // Subject line
									    text: 'Hello!\nThe user '+ user.username + " has " + type + " your request for the book " + userToBeNotified.notifications[userToBeNotified.notifications.length - 1].book.title + " by " + userToBeNotified.notifications[userToBeNotified.notifications.length - 1].book.author + ".\n For more information enter our website : http://10.100.100.38:1337"
									};
									// send mail with defined transport object
									transporter.sendMail(mailOptions);
								});
							}
						});
					}
				})(i);
			}
		}
	});
});

router.post('/read', function(request, result) {
	var users = db.get("users");
	var token = request.body.token;
	try {
		token = JSON.parse(token).token;
	} catch (e) {
		try {
			token = JSON.parse(token);
		} catch (exception) {}
	}
	var notificationID = request.body.notificationID;
	users.findOne({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === undefined || user === null) {
				result.status(404).end();
				return;
			}
			for (var i = 0; i < user.notifications.length; i++) {
				if (user.notifications[i].id === notificationID) {
					user.notifications[i].read = true;
					users.update({token : token}, {$set : {notifications : user.notifications}}, function() {
						result.status(200).end();
					});
					return;
				}
			}
		}
	});	
});

module.exports = router;

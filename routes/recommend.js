var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);

router.get('/', function(request, result) {
	var users = db.get("users");
	var books = db.get("books");
	var token = request.query.token;
	try {
		token = JSON.parse(token).token;
	} catch (e) {
		try {
			token = JSON.parse(token);
		} catch (e) {}
	}
	users.findOne({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === null || user === undefined) {
				result.status(404).end();
				return;
			}
			var preference = {}, i;
			for (i = 0; i < user.personalBooks.length; i++) {
				if (preference[user.personalBooks[i].category] === undefined || preference[user.personalBooks[i].category] === null) {
					preference[user.personalBooks[i].category] = 0;
				}
				preference[user.personalBooks[i].category]++;
			}
			for (i = 0; i < user.borrowedBooks.length; i++) {
				if (preference[user.borrowedBooks[i].category] === undefined || preference[user.borrowedBooks[i].category] === null) {
					preference[user.borrowedBooks[i].category] = 0;
				}
				preference[user.borrowedBooks[i].category]++;
			}
			var max = 0, indexKey = "";
			for (var key in preference) {
				if (preference[key] > max) {
					max = preference[key];
					indexKey = key;
				}
			}
			books.find({}, function(err, allBooks) {
				if (err) { result.send(err); }
				else {
					if (max === 0) {
						result.send(allBooks);
						return;
					}
					var booksToBeSent = [];
					for (var i = 0; i < allBooks.length; i++) {
						if (allBooks[i].category === indexKey) {
							booksToBeSent.push(allBooks[i]);
						}
					}
					result.send(booksToBeSent);
				}
			});
		}
	});
});

module.exports = router;
var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);
var nodemailer = require('nodemailer');

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'ibm.hackathon.mailer@gmail.com',
        pass: 'automailer11'
    }
});

router.get("/", function(request, result) {
  	var books = db.get("books");
  	var ISBN = request.query.ISBN;
  	books.findOne({ISBN : ISBN}, function(err, book) {
  		if (err) {}
  		else {
  			if (book === null || book === undefined) {
  				result.status(404).end();
  				return;
  			}
  			result.send(book);
  		}
  	});
});

router.get("/exceptpersonal", function(request, result) {
	var books = db.get("books");
	var users = db.get("users");
	var token = request.query.token;
	users.find({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === undefined || user === null) {
				result.status(404).end();
				return;
			}
			books.find({}, function(err, allBooks) {
				if (err) { result.send(err); }
				else {
					if (allBooks === undefined || allBooks === null || allBooks.length === 0) {
						result.status(404).end();
						return;
					}
					var booksToBeReturned = [];
					if (user.personalBooks === undefined || user.personalBooks === null) {
						result.send(allBooks);
						return;
					}
					for (var i = 0; i < user.personalBooks.length; i++) {
						for (var j = 0; j < allBooks.length; j++) {
							if (user.personalBooks[i].ISBN === allBooks[j].ISBN) {
								continue;
							}
							booksToBeReturned.push(allBooks[j]);
						}
					}
					result.send(booksToBeReturned);
				}
			});
		}
	});
});

router.get("/all", function(request, result) {
	var books = db.get("books");
	books.find({}, function(err, books) {
		if (err) { result.send(err); }
		else {
			result.send(books);
		}
	});
});

router.post("/addpersonal", function(request, result) {
	var books = db.get("books");
	var token = request.body.token;
	try {
		token = JSON.parse(token).token;
	} catch (e) {
		try {
			token = JSON.parse(token);
		} catch (exception) {}
	}
	var title = request.body.title;
	var author = request.body.author;
	var description = request.body.description;
	var category = request.body.category;
	var cover = request.body.cover;
	var ISBN = request.body.ISBN;
	if (cover === undefined || cover === null || cover === "") {
		cover = "http://www.bookfactory.com/_images/pop_blank_book_cover.jpg";
	}
	var bookToAdd = {
		title : title,
		author : author,
		description : description,
		category : category,
		cover : cover,
		ISBN : ISBN
	};
	var users = db.get("users");
	if (token === "-1") {
		result.status(401).end();
	}
	users.findOne({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === undefined || user === null) {
				result.status(404).end();
				return;
			}
			var personalBooks = user.personalBooks;
			if (personalBooks === undefined) {
				personalBooks = [];
			}
			personalBooks.push(bookToAdd);
			users.update({token : token}, {$set : {personalBooks : personalBooks}});
		}
	});
	books.findOne({ISBN : ISBN}, function(err, book) { 
		if (err) { result.send(err); }
		else {
			if (book !== null) {
				result.status(200).end();
				return;
			}
			books.insert(bookToAdd, function() {
				result.status(200).end();
			});
		}
	});
});

router.get("/users", function(request, result) {
	var users = db.get("users");
	var token = request.query.token;
	var ISBN = request.query.ISBN;
	users.findOne({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === undefined || user === null) {
				result.status(404).end();
				return;
			}
			users.find({}, function(err, allUsers) {
				if (err) { result.send(err); }
				else {
					if (allUsers === undefined || allUsers === null) {
						result.status(404).end();
						return;
					}
					if (allUsers.length === 0) {
						result.status(404).end();
						return;
					}
					var usersToSend = [];
					for (var i = 0; i < allUsers.length; i++) {
						if (allUsers[i].username === user.username) {
							continue;
						}
						for (var j = 0; j < allUsers[i].personalBooks.length; j++) {
							if (allUsers[i].personalBooks[j].ISBN === ISBN) {
								usersToSend.push({
									username : allUsers[i].username,
									firstName : allUsers[i].firstName,
									lastName : allUsers[i].lastName,
									city : allUsers[i].city,
									rating : allUsers[i].rating
								});
							}
						}
					}
					result.send(usersToSend);
				}
			});
		}
	});
});

router.post("/request", function(request, result) {
	var users = db.get("users");
	var books = db.get("books");
	var token = request.body.token;
	try {
		token = JSON.parse(token).token;
	} catch (e) {
		try {
			token = JSON.parse(token);
		} catch (exception) {}
	}
	var ISBN = request.body.ISBN;
	var username = request.body.username;
	users.findOne({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === undefined || user === null) {
				result.status(404).end();
				return;
			}
			users.findOne({username : username}, function(err, userToNotify) {
				if (err) { result.send(err); }
				else {
					if (userToNotify === null || userToNotify === undefined) {
						result.status(404).end();
						return;
					}
					books.findOne({ISBN : ISBN}, function(err, book) {
						if (err) { result.send(err); }
						else {
							if (book === null || book === undefined) {
								result.status(404).end();
								return;
							}
							var crypto = require("crypto");
							var current_date = (new Date()).valueOf().toString();
							var random = Math.random().toString();
							var hash = crypto.createHash('sha1').update(current_date + random).digest('hex');
							userToNotify.notifications.push({
								type : "request",
								id : hash,
								user : {
									username : user.username,
									firstName : user.firstName,
									lastName : user.lastName,
									rating : user.rating
								},
								book : {
									title : book.title,
									author : book.author,
									category : book.category
								},
								read : false
							});
							users.update({username : userToNotify.username}, {$set : {notifications : userToNotify.notifications}}, function() {
								result.status(200).end();
								var mailOptions = {
								    from: 'AutoMailer ✔ <ibm.hackathon.mailer@gmail.com', // sender address
								    to: userToNotify.email, // list of receivers
								    subject: 'Booksharing - You have a new request', // Subject line
								    text: 'Hello!\nThe user '+ user.username + " has requested your book : " + book.title + " by " + book.author + ".\nTo accept or decline this request, enter our website : http://10.100.100.38:1337"
								};
								// send mail with defined transport object
								transporter.sendMail(mailOptions);
							});
						}
					});
				}
			});
		}
	});
});

module.exports = router;

var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);

router.get('/', function(request, result) {
  	var token = request.query.token;
  	var single = request.query.single;
  	try {
  		token = JSON.parse(token).token;
  	} catch(e) {
  		try {
	  		token = JSON.parse(token);
	  	} catch(err) {}
  	}
  	if (token === '-1') {
  		result.status(401).end();
  		return;
  	}
  	var users = db.get('users');
  	users.findOne({token : token}, function(err, user) {
  		if (err) {}
  		else {
  			if (user === null || user === undefined) {
  				result.status(401).end();
  				return;
  			}
  			if (single === undefined) {
  				var object = {
  					username : user.username,
  					firstName : user.firstName,
  					lastName : user.lastName,
  					email : user.email,
                    phoneNumber : user.phoneNumber,
                    city : user.city,
                    rating : user.rating
  				};
  				result.send(object);
  			}
  			else {
                if (single === "notifications") {
                    var notificationsToBeSent = [];
                    for (var i = 0; i < user.notifications.length; i++) {
                        if (user.notifications[i].read === false) {
                            notificationsToBeSent.push(user.notifications[i]);
                        }
                    }
                    result.send(notificationsToBeSent);
                }
                else {
  				    result.send(user[single]);
                }
  			}
  		}
  	});
});

module.exports = router;
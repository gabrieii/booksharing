var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);

router.post('/', function(request, result) {
	var users = db.get("users");
	var token = request.body.token;
	try {
		token = JSON.parse(token).token;
	} catch (e) {
		try {
			token = JSON.parse(token);
		} catch(e) {}
	}
	var usernameToBeRated = request.body.username;
	var rating = request.body.rating;
	var notificationID = request.body.notificationID;
	users.findOne({token : token}, function(err, user) {
		if (err) { result.send(err); }
		else {
			if (user === null || user === undefined) {
				result.status(404).end();
				return;
			}
			users.findOne({username : usernameToBeRated}, function(err, userToBeRated) {
				if (err) { result.send(err); }
				else {
					if (userToBeRated === null || userToBeRated === undefined) {
						result.status(404).end();
						return;
					}
					userToBeRated.rating = (parseInt(rating) + parseInt(userToBeRated.ratingCounter) * parseInt(userToBeRated.rating)) / (parseInt(userToBeRated.ratingCounter) + 1);
					userToBeRated.ratingCounter = parseInt(userToBeRated.ratingCounter) + 1;
					for (var i = 0; i < user.notifications.length; i++) {
						if (user.notifications[i].id === notificationID) {
							user.notifications[i].read = true;
							users.update({username : user.username}, {$set : {rating : userToBeRated.rating.toString(), ratingCounter : userToBeRated.ratingCounter.toString(), notifications : user.notifications}});
							return;
						}
					}
				}
			});
		}
	});
});

module.exports = router;
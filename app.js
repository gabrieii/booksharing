var express = require('express');
var http = require('http');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var monk = require('monk');
var mongoUri = 'localhost:27017/IBM';
var db = monk(mongoUri);
var cors = require('cors');

var routes = require("./routes/index");
var login = require("./routes/login");
var logout = require("./routes/logout");
var signup = require("./routes/signup");
var userinfo = require("./routes/userinfo");
var book = require("./routes/book");
var notifications = require("./routes/notifications");
var rating = require("./routes/rating");
var recommend = require("./routes/recommend");
var clasament = require("./routes/clasament");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', 1337);

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use("/", routes);
app.use("/login", login);
app.use("/logout", logout);
app.use("/signup", signup);
app.use("/userinfo", userinfo);
app.use("/book", book);
app.use("/notifications", notifications);
app.use("/rating", rating);
app.use("/recommend", recommend);
app.use("/clasament", clasament);

var log4js = require('log4js'); // include log4js
log4js.configure({ // configure to use all types in different files.
    appenders: [
        {   
            type: "file",
            filename: "./error.log", // specify the path where u want logs folder info.log
            category: 'error'
        }
    ]
});

var errorLogger = log4js.getLogger('error'); // initialize the error logger

//db.get("users").update({}, {$set: {notifications : []}}, {multi : true});

//request pentru popularea bazei de date
function booksAPI() {
    var req = require("request");
    //j63z9jvguhx6syjxtazk8gy6 api key for usa today best sellers
    var pathForRequest = "http://api.usatoday.com/open/bestsellers/books/booklists?api_key=j63z9jvguhx6syjxtazk8gy6";
    req(pathForRequest, function(error, response, body) {
        var books = db.get("books");
        console.log(response.statusCode);
        if ( !error && response.statusCode === 200 ) {
            var booksAPI = JSON.parse(body).BookLists[0].BookListEntries;

            console.log(booksAPI[0]);
            for (var i = 0; i < booksAPI.length; i++) {
                (function(i) {
                    var pathForRequestISBN = "https://openlibrary.org/api/books?bibkeys=ISBN:" + booksAPI[i].ISBN + "&format=json&jscmd=details";
                    req(pathForRequestISBN, function(error, response, body) {
                        if ( !error && response.statusCode === 200 ) {
                            var specificBook = JSON.parse(body);
                            if (JSON.stringify(specificBook) === "{}") {
                                return;
                            }
                            var cover = specificBook["ISBN:" + booksAPI[i].ISBN].thumbnail_url;
                            if (cover !== undefined) {
                                var array = cover.split("-");
                                array[1] = "L.jpg";
                                cover = array[0] + "-" + array[1];
                                console.log(cover);
                            }
                            else {
                                cover = "http://www.bookfactory.com/_images/pop_blank_book_cover.jpg";
                            }
                            books.insert({
                                title : booksAPI[i].Title,
                                author : booksAPI[i].Author,
                                description : booksAPI[i].BriefDescription,
                                category : booksAPI[i].Category.CategoryName,
                                cover : cover,
                                ISBN : booksAPI[i].ISBN,
                            });
                        }
                    });
                })(i);
            }
        }
    });
}

//booksAPI();

function populeaza() {
    var users = db.get("users");
    var books = db.get("books");
    var async = require("async");
    async.series([
        function(callback) {
            users.find({}, function(err, allUsers) {
                callback(null, allUsers);
            });
        },
        function(callback) {
            books.find({}, function(err, allBooks) {
                callback(null, allBooks);
            });
        }
    ], function(err, result) {
        var allUsers = result[0];
        var allBooks = result[1];
        for (var i = 0; i < allUsers.length; i++) {
            (function(i) {
                for (var j = 0; j < 4; j++) {
                    if (allUsers[i].personalBooks === undefined || allUsers[i].personalBooks === null) {
                        allUsers[i].personalBooks = [];
                    }
                    var randomGeneratedBook = allBooks[Math.round(Math.random() * allBooks.length)];
                    allUsers[i].personalBooks.push({
                        title : randomGeneratedBook.title,
                        author : randomGeneratedBook.author,
                        description : randomGeneratedBook.description,
                        category : randomGeneratedBook.category,
                        cover : randomGeneratedBook.cover,
                        ISBN : randomGeneratedBook.ISBN
                    });
                }
                users.update({username : allUsers[i].username}, {$set : {personalBooks : allUsers[i].personalBooks}});
            })(i);
        }
    });
}

//populeaza();

app.get("/cities", function(request, result) {
    var orase = "Abrud,Adjud,Aiud,Alba Iulia,Aleşd,Alexandria,Agnita,Anina,Aninoasa,Arad,Avrig,Azuga,Babadag,Bacău,Baia de Aramă,Baia de Arieş,Baia Mare,Baia Sprie,Balş,Baraolt,Băicoi,Băile Govora,Băile Herculane,Băile Olăneşti,Băileşti,Băile Tuşnad,Bălan,Bârlad,Bocşa,Borsec,Beclean,Beiuş,Bereşti,Bicaz,Bistriţa,Blaj,Boldeşti-Scăieni,Bolintin-Vale,Borşa,Botoşani,Brad,Braşov,Brăila,Breaza,Brezoi,Bucureşti,Budeşti,Buftea,Buhuşi,Bumbeşti-Jiu,Buşteni,Buziaş,Buzău,Calafat,Carei,Caracal,Caransebeş,Cavnic,Călan,Călăraşi,Călimăneşti-Căciulata,Câmpeni,Câmpia Turzii,Câmpina,Câmpulung,Câmpulung Moldovenesc,Cehu Silvaniei,Cernavodă,Chişineu-Criş,Cisnădie,Cluj-Napoca,Codlea,Comarnic,Comăneşti,Constanţa,Copşa Mică,Corabia,Costeşti,Covasna,Craiova,Cristuru Secuiesc,Cugir,Curtici,Curtea de Argeş,Darabani,Dărmăneşti,Dealu,Dej,Deta,Deva,Dorohoi,Drăgăneşti-Olt,Drăgăşani,Drobeta-Turnu Severin,Dumbrăveni,Eforie,Făgăraş,Făget,Fălticeni,Făurei,Feteşti,Fieni,Filiaşi,Focşani,Fundulea,Galaţi,Găeşti,Geoagiu,Gheorgheni,Gherla,Giurgiu,Gura Humorului,Haţeg,Hârlău,Hârşova,Horezu,Huedin,Hunedoara,Huşi,Ianca,Iaşi,Iernut,Ineu,Isaccea,Însurăţei,Întorsura Buzăului,Jibou,Jimbolia,Lehliu Gară,Lipova,Luduş,Lugoj,Lupeni,Mangalia,Marghita,Măcin,Mărăşeşti,Medgidia,Mediaş,Miercurea Ciuc,Mihăileşti,Mioveni,Mizil,Moineşti,Moldova Nouă,Moreni,Motru,Murfatlar,Nădlac,Năsăud,Năvodari,Negreşti,Negreşti-Oaş,Negru Vodă,Nehoiu,Novaci,Nucet,Ocna Mureş,Ocna Sibiului,Ocnele Mari,Odobeşti,Odorheiu Secuiesc,Olteniţa,Oneşti,Oradea,Oraviţa,Orăştie,Orşova,Otopeni,Oţelu Roşu,Ovidiu,Panciu,Paşcani,Pâncota,Petrila,Petroşani,Piatra Neamţ,Piatra Olt,Piteşti,Ploieşti,Plopeni,Pogoanele,Predeal,Pucioasa,Rădăuţi,Râmnicu Sărat,Râmnicu Vâlcea,Râşnov,Reghin,Reşiţa,Roman,Roşiori de Vede,Rovinari,Rupea,Salonta,Satu Mare,Săcele,Săveni,Sângeorz-Băi,Sânnicolau Mare,Scorniceşti,Sebeş,Sebiş,Seini,Segarcea,Sfântu Gheorghe,Sibiu,Sighetu Marmaţiei,Sighişoara,Simeria,Sinaia,Siret,Slatina,Slănic,Slănic Moldova,Slobozia,Solca,Sovata,Strehaia,Suceava,Sulina,Şimleu Silvaniei,Ştei,Tălmaciu,Tăşnad,Târgovişte,Târgu Bujor,Târgu Cărbuneşti,Târgu Frumos,Târgu Jiu,Târgu Lăpuş,Târgu Mureş,Târgu Neamţ,Târgu Ocna,Târgu Secuiesc,Târnăveni,Techirghiol,Tecuci,Teiuş,Timişoara,Tismana,Titu,Topliţa,Topoloveni,Tulcea,Turda,Turnu Măgurele,Ţăndărei,Ţicleni,Uricani,Urlaţi,Urziceni,Valea lui Mihai,Vaslui,Vaşcău,Vatra Dornei,Vălenii de Munte,Vânju Mare,Victoria,Videle,Vişeu de Sus,Vlăhiţa,Vulcan,Zalău,Zărneşti,Zimnicea,Zlatna";
    var array = orase.split(",");
    result.send(array);
});

//fara liniile astea 3, nimic nu s-ar intampla...
//porneste serverul
http.createServer(app).listen(app.get('port'), function(){
    console.log('Server listening on port ' + app.get('port'));
});

module.exports = app;